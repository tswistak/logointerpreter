#LogoInterpreter

Logo (Polish version) interpreter written in python+pygame. No turtle and cmd are used.

##Usage
Run in cmd: "python logo.py <filename>", e.g. "python logo.py test.txt". Currently REPL mode isn't implemented.

Written and tested under Python 3.3.3 and PyGame 1.9.2a0. 

List of commands is available in COMMANDS.TXT along with usage comments.

##Already working

- interpreting files
- (most of) drawing functions
- loops, conditions

##To do

- user defined functions and variables
- support for Polish diacritics
- functions: showing turtle, fillng objects with color
- REPL

##License
Licensed under the MIT license.