import utils
from commands import *

FUNCTIONS_NOARG={'pz': pz, 'sz': sz, 'wroc': wroc, 'podnies': pod, 'pod': pod, 'opusc': opu, 'opu': opu, 'zmaz': zmaz, \
    'czysc': cs, 'cs': cs, 'scieranie': scier, 'scier': scier, 'stop': stop}
FUNCTIONS_ARG={'naprzod': np, 'np': np, 'wstecz': ws, 'ws': ws, 'prawo': pw, 'pw': pw, 'lewo': lw, 'lw': lw, \
    'ustalkolorpisaka': ukp, 'ukp': ukp, 'ustaltlo': ut, 'ut': ut, 'ustalgruboscpisaka': ugp, 'ugp': ugp, 'czekaj': czekaj}
SYNTAX={'powtorz', 'oto', 'jesli'}
USER_DEF_ARG={} # 'name' -> (linenumber,[args])
USER_DEF_NOARG={} # 'name' -> linenumber
USER_VARS={} # 'name' -> value

def varToValue(text):
    for k,v in USER_VARS.items():
        text=text.replace(k,str(eval(utils.commaToDot(v))))
    return text

def interpret(line,lineNumber):
    if not vars.endProgram and not vars.stopProcedure:
        line=line.lower()
        splitted=line.split(' ')
        splitted=utils.mergeBlocks(splitted)
        if splitted[0] in FUNCTIONS_ARG:
            if len(splitted)>1:
                # to do: support for blocks
                FUNCTIONS_ARG[splitted[0]](eval(utils.commaToDot(varToValue(splitted[1]))))
            else:
                print('Not enough arguments for '+splitted[0])
        elif splitted[0] in FUNCTIONS_NOARG:
            FUNCTIONS_NOARG[splitted[0]]()
        elif splitted[0] in SYNTAX:
            if splitted[0]=='powtorz':
                # POWTORZ (times) [ code ]
                if len(splitted)>2:
                    powtorz(int(eval(utils.commaToDot(splitted[1]))),splitted[2])
                else:
                    print('Incomplete loop')
            elif splitted[0]=='jesli':
                # JESLI (condition) [ true ] [ false ]
                if len(splitted)>2:
                    tmp=utils.mergeBlocks(splitted[1:len(splitted)])
                    if len(tmp)==2:
                        jesli(tmp[0],tmp[1])
                    elif len(tmp)>2:
                        jesli(tmp[0],tmp[1],tmp[2])
                    else:
                        print('Incomplete condition')
                else:
                    print('Incomplete condition')
        elif splitted[0] in USER_DEF_ARG:
            argNum=len(USER_DEF_ARG[splitted[0]][1])
            args=[]
            if len(splitted)>argNum:
                for i in range(argNum):
                    args.append(spliited[1+i])
                runFunction(splitted[0],args)
            else:
                print('Wrong number of arguments for '+splitted[0])
        elif splitted[0] in USER_DEF_NOARG:
            runFunction(splitted[0])
        else:
            print(splitted[0]+' is not recognized')

def splitLine(line):
    splitted=line.split(' ')
    splitted=utils.mergeBlocks(splitted)
    result=[]
    length=len(splitted)
    i=0
    while i<length:
        splitted[i]=splitted[i].replace('\n','')
        if (splitted[i] in FUNCTIONS_ARG):
            # every function in this map have only one argument (int or arithmetic operation)
            if i<length-1:
                try:
                    eval(utils.commaToDot(varToValue(splitted[i+1]))) # check if its legal argument
                    result.append(splitted[i]+' '+splitted[i+1])
                    i+=1
                except ValueError:
                    print('Illegal argument for '+splitted[i])
            else:
                print('Not enough arguments for '+splitted[i])
        elif (splitted[i] in FUNCTIONS_NOARG) or (splitted[i] in USER_DEF_NOARG):
            result.append(splitted[i])
        elif splitted[i] in USER_DEF_ARG:
            # seperate from FUNCTIONS_ARG because it can have many arguments
            if i<length-USER_DEF_ARG[splitted[i]][1]:
                tmp=splitted[i]
                for j in range(1,USER_DEF_ARG[splitted[i]][1]):
                    try:
                        eval(utils.commaToDot(varToValue(splitted[i+j]))) # check if its legal argument
                        tmp+=' '+splitted[i+j]
                        i+=1
                    except ValueError:
                        print('Illegal argument for '+splitted[i])
                        tmp=''
                        break
                if tmp!='':
                    result.append(tmp)
            else:
                print('Not enough arguments for '+splitted[i])
        elif splitted[i] in SYNTAX:
            if splitted[i]=='powtorz':
                if i<length-2:
                    try:
                        eval(utils.commaToDot(varToValue(splitted[i+1]))) # check if its legal argument
                        result.append(splitted[i]+' '+splitted[i+1]+' '+splitted[i+2])
                        i+=2
                    except ValueError:
                        print('Illegal argument for '+splitted[i])
                else:
                    print('Not enough arguments for '+splitted[i])
            elif splitted[i]=='jesli':
                if i<length-2:
                    try:
                        eval(utils.commaToDot(varToValue(splitted[i+1]))) # check if its legal argument
                        tmp=splitted[i]+' '+splitted[i+1]+' '+splitted[i+2]
                        i+=2
                        # optional ELSE argument
                        if i<length-1 and '[' in splitted[i+1] and ']' in splitted[i+1]:
                            tmp+=' '+splitted[i+1]
                            i+=1
                        result.append(tmp)
                    except ValueError:
                        print('Illegal argument for '+splitted[i])
                else:
                    print('Not enough arguments for '+splitted[i])
            # place for more syntax
            else:
                # function definition is the only thing that can be in line
                tmp=''
                for j in range(i,length):
                    tmp+=splitted[j]+' '
                result.append(tmp)
                break
        elif splitted[i]!='':
            print(splitted[i]+' is not recognized...')
        i+=1
    return result      

def runFunction(name,argVals=[]):
    lineNum=0
    args=[]
    variables={}
    isRecursion=False # generally to check calls to another user defined functions
    vars.inProcedure=True
    if name in USER_DEF_ARG:
        # 'name' -> (linenumber,[argnames])
        lineNum,args=USER_DEF_ARG[name]
        if len(argVals)!=len(args):
            print('Wrong number of arguments for '+name)
            return
        variables=dict(zip(args,argVals))
    else:
        lineNum=USER_DEF_NOARG[name]
    lineNum+=1 # lineNum is set to function definition, we want a next line
    USER_VARS.update(variables)
    # execute function's code
    while not vars.stopProcedure and not vars.endProgram and lineNum<len(vars.lines):
        line=interpreter.splitLine(vars.lines[lineNum])
        for i in range(len(line)):
            # check if it's end of procedure
            if 'juz' in line[i]:
                vars.stopProcedure=True
                break
            # check if there's going to be a call to another user def function
            isRecursion=line[i] in USER_DEF_ARG or line[i] in USER_DEF_NOARG
            interpreter.interpret(line[i],lineNum)
            # revert variables
            if isRecursion:
                USER_VARS.update(variables)
                isRecursion=False
                vars.stopProcedure=False
                vars.inProcedure=True
        lineNum+=1
        utils.pygameUpdate()
    # delete temporary vars from USER_VARS
    for key in iter(variables):
        del USER_VARS[key]
    vars.stopProcedure=False
    vars.inProcedure=False