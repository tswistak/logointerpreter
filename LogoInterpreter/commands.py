import vars,utils,interpreter

# NAPRZOD / NP - go straight
def np(steps):
    for i in range(steps):
        utils.putPixel(vars.penPosition)
        position=utils.nextStep()
        vars.penPosition=vars.penPosition[0]+position[0],vars.penPosition[1]+position[1]
        utils.drawTurtle()
        utils.pygameUpdate()

# WSTECZ / WS - go back
def ws(steps):
    for i in range(steps):
        utils.putPixel(vars.penPosition)
        position=utils.nextStep()
        vars.penPosition=vars.penPosition[0]-position[0],vars.penPosition[1]-position[1]
        utils.drawTurtle()
        utils.pygameUpdate()

# PRAWO / PW - turn right
def pw(angle):
    vars.penAngle=(vars.penAngle+angle)%360

# LEWO / LW - turn left
def lw(angle):
    vars.penAngle=(vars.penAngle-angle)%360

# PZ - show turtle
def pz():
    vars.penVisible=True

# SZ - hide turtle
def sz():
    vars.penVisible=False

# WROC - sets pen to start position
def wroc():
    vars.penPosition=(vars.WIDTH/2,vars.HEIGHT/2)
    vars.penAngle=0

# PODNIES / POD - raise pen
def pod():
    vars.penDown = False

# OPUSC / OPU - lower pen
def opu():
    vars.penDown = True

# ZMAZ - clear screen
def zmaz():
    vars.DISPLAYSURF.fill(vars.bgColor)
    vars.DRAWSURF.fill(vars.TRANSPARENT_COLOR)

# CZYSC / CS - clear screen and reset pen
def cs():
    zmaz()
    wroc()

# USTALKOLORPISAKA / UKP - set pen color
def ukp(color):
    # to do: using RGB values from input: UKP [R G B]
    vars.penColor=vars.COLOR[color]

# USTALTLO / UT - change background color
def ut(color):
    # to do: using RGB values from input: UT [R G B]
    vars.bgColor=vars.COLOR[color]
    vars.DISPLAYSURF.fill(vars.bgColor)

# USTALGRUBOSCPISAKA / UGP - set pen width
def ugp(width):
    vars.penWidth = width

# SCIERANIE / SCIER - set pen to eraser
def scier():
    vars.penIsEraser=True

# ZAMALUJ - fill with pen color
def zamaluj():
    pass

# CZEKAJ - wait
def czekaj(time):
    pygame.time.wait(time)

# POWTORZ - loop
def powtorz(times,commands):
    lines=interpreter.splitLine(utils.removeBrackets(commands))
    for i in range(times):
        for j in range(len(lines)):
            interpreter.interpret(lines[j],vars.programCounter)

# OTO - procedure definition
def oto(name,arguments,lineNumber):
    if ':' in arguments:
        pass
    else:
        interpreter.USER_DEF_NOARG[name]=lineNumber

# JESLI - condition
def jesli(condition,codeTrue,codeFalse=''):
    if eval(utils.commaToDot(condition)):
        lines=interpreter.splitLine(utils.removeBrackets(codeTrue))
        for i in range(len(lines)):
            interpreter.interpret(lines[i],vars.programCounter)
    elif codeFalse!='':
        lines=interpreter.splitLine(utils.removeBrackets(codeFalse))
        for i in range(len(lines)):
            interpreter.interpret(lines[i],vars.programCounter)

# STOP - end of program
def stop():
    if inProcedure:
        vars.stopProcedure=True
    else:
        vars.endProgram=True

