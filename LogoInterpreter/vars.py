# consts
FPS=60
WIDTH=640
HEIGHT=480
COLOR=[(0,0,0), (0,0,128), (0,128,0), (0,128,128), (128,0,0), (128,0,128), (128,128,0), (192,192,255), \
       (128,128,128), (0,0,255), (0,255,0), (0,255,255), (255,0,0), (255,0,255), (255,255,0), (255,255,255)]
TRANSPARENT_COLOR=(14,3,92)

# pygame
FPSCLOCK=0
DISPLAYSURF=0
DRAWSURF=0
TURTLESURF=0

# drawing
penPosition=(int(WIDTH/2),int(HEIGHT/2))
penDown=True
penVisible=False
penIsEraser=False
penWidth=1
penAngle=0
penColor=(0,0,0)
bgColor=(255,255,255)

# interpreter control
lines=[]
programCounter=0
endProgram=False
inProcedure=False
stopProcedure=False