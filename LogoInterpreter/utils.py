import pygame,pygame.locals
import math,re,sys
import vars

# setups pygame
def pygameSetup():
    pygame.init()
    pygame.display.set_caption('Logo Interpreter')
    vars.FPSCLOCK=pygame.time.Clock()
    vars.DISPLAYSURF=pygame.display.set_mode((vars.WIDTH,vars.HEIGHT))
    vars.DISPLAYSURF.fill(vars.bgColor)
    vars.DRAWSURF=pygame.Surface((vars.WIDTH,vars.HEIGHT))
    vars.DRAWSURF.fill(vars.TRANSPARENT_COLOR)
    vars.DRAWSURF.set_colorkey(vars.TRANSPARENT_COLOR)
    vars.TURTLESURF=pygame.Surface((vars.WIDTH,vars.HEIGHT))
    vars.TURTLESURF.fill(vars.TRANSPARENT_COLOR)
    vars.TURTLESURF.set_colorkey(vars.TRANSPARENT_COLOR)

# updates screen
def pygameUpdate():
    # merge all surfaces
    vars.DISPLAYSURF.fill(vars.bgColor)
    vars.DISPLAYSURF.blit(vars.DRAWSURF,(0,0))
    if vars.penVisible:
        vars.DISPLAYSURF.blit(vars.TURTLESURF,(0,0))
    for event in pygame.event.get():
        # stop application on clicking exit button
        if event.type==pygame.locals.QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
    vars.FPSCLOCK.tick(vars.FPS)

# calculates turtles next position
def nextStep():
    angle=math.radians((vars.penAngle-90)%360)
    x=-math.cos(angle)
    y=math.sin(angle)
    return x,y

# puts pixel on drawsurf
def putPixel(position):
    if vars.penDown:
        pixObj = pygame.PixelArray(vars.DRAWSURF)
        pixObj[int(position[0])][int(position[1])]=vars.penColor
        del pixObj

# draws a turtle
def drawTurtle():
    if vars.penVisible==True:
        vars.TURTLESURF.fill(vars.TRANSPARENT_COLOR)
        # to do...

# replaces all commas to dots
def commaToDot(text):
    return text.replace(',','.')

# removes square brackets
def removeBrackets(text):
    #return re.sub('[\[\]]','',text)
    if text[len(text)-1]==' ' or text[len(text)-1]=='\n':
        return text[1:len(text)-2]
    else:
        return text[1:len(text)-1]
    

# merges blocks into one list element 
# i.e. [...,'[np','50','pz]',...] -> [...,'[np 50 pz]',...]
def mergeBlocks(list):
    result=[]
    block=''
    blocksStarted=0
    for i in range(len(list)):
        if '[' in list[i]:
            block+=list[i]
            if not ']' in list[i]:
                block+=' '
                blocksStarted+=1
            elif blocksStarted==0:
                result.append(block)
                block=''
            else:
                block+=' '
        elif blocksStarted>0:
            block+=list[i]
            if ']' in list[i]:
                blocksStarted-=1
                if blocksStarted==0:
                    result.append(block)
                    block=''
                else:
                    block+=' '
            else:
                block+=' '
        else:
            block=''
            result.append(list[i])
    return result