import sys
import vars,utils,interpreter

def main(file=''):
    # load file
    fileLoaded=False
    if file!='':
        f=open(file)
        print('Loading file')
        vars.lines=list(f)
        f.close()
        fileLoaded=True
    utils.pygameSetup()
    # main loop
    vars.programCounter=0
    while True:
        if fileLoaded and (len(vars.lines)>vars.programCounter) and not vars.endProgram:
            line=interpreter.splitLine(vars.lines[vars.programCounter])
            for i in range(len(line)):
                interpreter.interpret(line[i],vars.programCounter)
            vars.programCounter+=1
        else:
            pass # to do - repl
        utils.pygameUpdate()

if __name__ == '__main__':
    if len(sys.argv)>1:
        main(sys.argv[1])
    else:
        #print('Usage: logo.py <filename>')
        main()
    #main()