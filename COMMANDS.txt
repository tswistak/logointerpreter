Usage comments:
Original language generally has no rules about syntax. That's why it was possible to write: "np 20 + 30 pw 30" what was interpreted the same as "np 50 pw 30" or "np 20+30 pw 30". In my interpreter "np 20 + 30 pw 30" would be interpreted as "np 20 pw 30" because it splits multiple functions in lines by spaces ("np 20+30" is working). For now, I don't think about changing it - it's not urgent matter for me.
Also, for now I don't support Polish diacritics, that's why all "�,�,�.." needs to be written as "a,e,c...". 
For this moment, interpreter mostly works with drawing (turtle graphics). Commands and syntax elements not connected with it, probably won't be implemented. Support for multiple turtles also isn't planned.

Working commands:
- naprzod / np (step number) - go forward
- wstecz / ws (step number) - go backward
- prawo / pw (angle) - turn right
- lewo / lw (angle) - turn left
- podnies / pod - raise pen (don't draw)
- opusc / opu - lower pen (draw)
- zmaz - clears screen without changing turtle's position
- wroc - change's turtle position to center of a screen
- czysc / cs - clears screen and changes turtle position to center of a screen
- czekaj (time) - waits given amount of time
- powtorz (times) [(commands)] - loop
- jesli (condition) [(commands for true)] [(OPTIONAL - commands for false)] - condition

Half-working commands:
- stop - break from procedure (currently breaks from interpreting file)
- ustalkolorpisaka / ukp (color) - sets pen color (currently only predefined values 0-15 are working, RGB blocks doesn't)
- ustaltlo / ut (color) - sets background color (the same problem as in ukp)

Not working commands (in development/planned):
- pz - show turtle
- sz - hide turtle
- scieranie / scier - changes turtle into eraser
- oto - function definition 
- przypisz / przyp "(name) (value) - variable definition
- ustalgruboscpisaka / ugp (width) - sets strokes width (originally entering only ugp would show a window with choosing width - it's not planned here)
- ustalkolormalowania / ukm (color) - sets filling color
- zamaluj - flood fills area underneath turtle
- zapiszek (filename) - save drawing to BMP file

Not working commands (not planned):
- oknozolwia - doesnt wrap turtle when going outside screen
- sklej - wrap turtle when going outside screen
- pisz / ps (variable) - prints contents of variable
- wpisz (variable) - the same as "pisz" but without goint to new line
- pwk (value) - returns sqaure root of value
- ufl (value) - sets how many digits after a dot will be remembered in decimal values
- red "(procedure) - runs procedure editor
- wynik / wy (value) - returns value in function
- czytajznak / cz - read char from keyboard
- czytajliste / cl - read text from keyboard
- pierw (list) - returns first element from a list
- ost (list) - returns last element from a list
- bezpierw / bp (list) - returns list without first element
- bezost / bo (list) - returns list without last element
- nap (list1) (list2) - concatenates list2 to list1
- nak (list1) (list2) - concatenates list1 to list2
- puste? (list) - returns if list is empty
- sluchaj (name) - changes turtle
- kazdy [(commands)] - sends commands to every turtle
- pokaz (name) - shows object
- kto - returns name(s) of current turtle(s)
- wszystkie - returns names of all turtles
- usunzolwia / usz (name) - removes turtle
- ... and probably more which I don't remember ...